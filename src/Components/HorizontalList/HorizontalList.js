import React, { Component } from 'react';
import './HorizontalList.css';
import { GridList, GridListTile, GridListTileBar } from '@material-ui/core';
import { Link } from 'react-router-dom';

class HorizontalList extends Component {
    state = {};

    render() {
        const { listData, title } = this.props;

        return (
            <div className="horizontal-list-container">
                <h1>{title}</h1>
                <div className="horizontal-list">
                    <GridList className="grid-list" cols={2.5}>
                        {listData.map((tile, index) => (
                            <GridListTile key={index}>
                                <Link
                                    to={{
                                        pathname: '/day',
                                        state: {
                                            pageName: `${title}_${tile.title}`
                                        }
                                    }}
                                >
                                    <div className="tile-content" />
                                    <GridListTileBar
                                        title={tile.title}
                                        className="title-bar title"
                                    />
                                </Link>
                            </GridListTile>
                        ))}
                    </GridList>
                </div>
            </div>
        );
    }
}

export default HorizontalList;
