import React, { Component } from 'react';
import './App.css';
import MenuIcon from '@material-ui/icons/Menu';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import Search from '@material-ui/icons/Search';
import { AppBar, Toolbar, Typography, IconButton } from '@material-ui/core';
import MainPage from './Pages/MainPage/MainPage';
import { Switch, Route } from 'react-router';
import DayPage from './Pages/DayPage/DayPage';
import ListPage from './Pages/ListPage/ListPage';
import { withRouter } from 'react-router';

class App extends Component {
    render() {
        const { location, history } = this.props;
        const locationStatePage = location.state && location.state.pageName;
        const pageTitle = locationStatePage
            ? locationStatePage
            : 'Meet Mindera';
        const icon = locationStatePage ? (
            <ArrowBackIcon onClick={history.goBack} />
        ) : (
            <MenuIcon />
        );

        return (
            <div className="App">
                <AppBar position="static">
                    <Toolbar>
                        <IconButton
                            className="menu-button"
                            color="inherit"
                            aria-label="Menu"
                        >
                            {icon}
                        </IconButton>
                        <Typography
                            variant="title"
                            color="inherit"
                            className="flex"
                        >
                            {pageTitle}
                        </Typography>

                        <IconButton
                            className="menu-button"
                            color="inherit"
                            aria-label="Search"
                        >
                            <Search />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Switch>
                    <Route exact path="/" component={MainPage} />
                    <Route path="/day" component={DayPage} />
                    <Route path="/list" component={ListPage} />
                </Switch>
            </div>
        );
    }
}

export default withRouter(App);
