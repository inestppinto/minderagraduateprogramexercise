import React, { Component } from 'react';
import './DayPage.css';
import { List, ListItem, ListItemText, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';

class DayPage extends Component {
    state = {};
    render() {
        const data = [
            'List 01',
            'List 02',
            'List 03',
            'List 04',
            'List 05',
            'List 06',
            'List 07',
            'List 08',
            'List 09'
        ];
        return (
            <div className="day-page-container">
                {data.map((dataTitle, index) => (
                    <List key={index} component="nav">
                        <Link
                            to={{
                                pathname: '/list',
                                state: {
                                    pageName: dataTitle
                                }
                            }}
                        >
                            <ListItem button>
                                <ListItemText primary={dataTitle} />
                            </ListItem>
                        </Link>
                        <Divider />
                    </List>
                ))}
            </div>
        );
    }
}

export default DayPage;
