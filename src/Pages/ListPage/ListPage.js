import React, { Component } from 'react';
import './ListPage.css';
import { GridList, GridListTile, GridListTileBar } from '@material-ui/core';

class ListPage extends Component {
    state = {};

    render() {
        const titlesData = [
            'Description 01',
            'Description 02',
            'Description 03',
            'Description 04',
            'Description 05',
            'Description 06'
        ];
        return (
            <div className="list-page-container">
                <GridList
                    cellHeight={180}
                    cols={2}
                    className="grid-list-page"
                    spacing={10}
                >
                    {titlesData.map((title, index) => (
                        <GridListTile
                            className="grid-list-page-tile"
                            key={index}
                        >
                            <div className="list-square" />
                            <GridListTileBar
                                className="grid-list-title-bar"
                                title={title}
                            />
                        </GridListTile>
                    ))}
                </GridList>
            </div>
        );
    }
}

export default ListPage;
