import './EventsPage.css';
import React, { Component } from 'react';
import HorizontalList from '../../Components/HorizontalList/HorizontalList';

class EventsPage extends Component {
    state = {};
    render() {
        const data = [
            {
                listTitle: "Open Day '18",
                listData: [
                    { title: 'Day 0' },
                    { title: 'Day 1' },
                    { title: 'Day 2' },
                    { title: 'Day 3' }
                ]
            },
            {
                listTitle: 'Graduate Program',
                listData: [
                    { title: 'Inês' },
                    { title: '' },
                    { title: '' },
                    { title: '' }
                ]
            },
            {
                listTitle: 'Meet Mindera Code & Culture',
                listData: [
                    { title: '' },
                    { title: '' },
                    { title: '' },
                    { title: '' }
                ]
            }
        ];
        return (
            <div>
                <img className="event-image" alt="" />
                {data.map((dataList, index) => (
                    <HorizontalList
                        title={dataList.listTitle}
                        listData={dataList.listData}
                        key={index}
                    />
                ))}
            </div>
        );
    }
}

export default EventsPage;
