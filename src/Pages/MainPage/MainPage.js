import React, { Component } from 'react';
import { AppBar, Tabs, Tab } from '@material-ui/core';
import EventsPage from '../EventsPage/EventsPage';

class MainPage extends Component {
    state = {
        value: 0
    };

    handleChange = (event, value) => {
        this.setState({ value });
    };

    render() {
        const { value } = this.state;
        return (
            <div>
                <AppBar position="static">
                    <Tabs value={value} onChange={this.handleChange} fullWidth>
                        <Tab label="Events" />
                        <Tab label="Vacancies" />
                    </Tabs>
                </AppBar>
                {value === 0 && <EventsPage />}
                {value === 1 && <div>Mindera vacancies </div>}
            </div>
        );
    }
}

export default MainPage;
