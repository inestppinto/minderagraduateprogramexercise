# Mindera Graduate Program - Exercise a)

This project was created with [Create React App][createReact]. To run this application you must open the console and run the following commands:
```sh
$ npm install
$ npm run start
```

[createReact]: <https://reactjs.org/docs/add-react-to-a-new-app.html>